/**
 * cordova Web Intent plugin
 *
 */
var exec = require('cordova/exec');
var EMLauncherIntentSender = {

	startActivityForResult: function(params, success, fail) {
        return exec(function(args) {
            success(args);
        }, function(args) {
            fail(args);
        }, 'EMLauncherIntentSender', 'startActivityForResult', [params]);
    }
}
module.exports = EMLauncherIntentSender;    
