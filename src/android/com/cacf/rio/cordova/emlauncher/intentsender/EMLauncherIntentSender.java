package com.cacf.rio.cordova.emlauncher.intentsender;

import java.util.HashMap;
import java.util.Map;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaResourceApi;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.util.Log;

/**
 * Permet a une application de lancer CDIGIT en passant un context
 *
 * @author tpocchio
 *
 */
public class EMLauncherIntentSender extends CordovaPlugin {
    private static final String CDIGIT_INTENT_NAME = "com.cacf.rio.cordova.emlauncher.LAUNCH_CDIGIT";
	private static final String LOG_TAG = "LOG EMLauncherIntentSender";
    private static final String CDIGIT_CONTEXT = "CDIGIT_CONTEXT";
    private static final String CDIGIT_RESULT = "CDIGIT_RESULT";
    private final int CDIGIT_INTENT_REQUEST_CODE = 0x133EFB6;
    private static String installReferrer = null;
    private CallbackContext callbackContext = null;

    /**
     * @return true iff if the action was executed successfully, else false.
     */
    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) {
    	this.callbackContext = callbackContext;
    	
        try {
        	
        	Log.i(LOG_TAG, String.format("execute action: %s", action));
        	
        	if (args!=null) {
        		Log.i(LOG_TAG, String.format("execute args: %s", args.toString()));
        	}else {
        		Log.i(LOG_TAG, "execute args is null");
        	}
        	
            if ("startActivityForResult".equals(action)) {
            	Log.i(LOG_TAG, "execute startActivity");
                if (args.length() != 1) {
                	Log.i(LOG_TAG, "execute args length != 1");
                    callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.INVALID_ACTION));
                    return false;
                }
                // Parse the arguments
                final CordovaResourceApi resourceApi = webView.getResourceApi();
                JSONObject obj = args.getJSONObject(0);
                JSONObject extras = obj.has(CDIGIT_CONTEXT) ? obj.getJSONObject(CDIGIT_CONTEXT) : null;
                
                Map<String, String> extrasMap = new HashMap<String, String>();
                
                if (extras!=null) {
                	extrasMap.put(CDIGIT_CONTEXT, extras.toString());
                	Log.i(LOG_TAG, "execute extrasMap put "+CDIGIT_CONTEXT+"="+extras.toString());
                }else {
                	Log.i(LOG_TAG, "execute extras is NULL");
                }
                
                startActivityForResult(extrasMap);
                //callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK));
                return true;
            }
            callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.INVALID_ACTION));
            return false;
        } catch (JSONException e) {
            final String errorMessage = e.getMessage();
            Log.e(LOG_TAG, errorMessage);
            callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.JSON_EXCEPTION, errorMessage));
            return false;
        }
    }

    /**
     * constuit un intent pour lancer CDIGIT
     */
    private Intent buildCdigitIntent(Map<String, String> extras) {
    	Intent newIntent = new Intent(CDIGIT_INTENT_NAME);

    	//newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |Intent.FLAG_ACTIVITY_CLEAR_TASK);
    	
        for (Map.Entry<String, String> entry : extras.entrySet()) {
            final String key = entry.getKey();
            final String value = entry.getValue();

            newIntent.putExtra(key, value);
        }

        return newIntent;
    }
    
    
    /**
     * lance l'intent en passant un intent code (CDIGIT_INTENT_REQUEST_CODE) pour que cdigit puisse répondre et
     * renvoyer vers la méthode onActivityResult de cette classe
     */
    void startActivityForResult(Map<String, String> extras) {

    	final CordovaPlugin callerPlugin = this;
    	final Intent cdigitIntent = buildCdigitIntent(extras);
    	
        cordova.getThreadPool().execute(
            new Runnable() {
                public void run() {
                    try {
                        callerPlugin.cordova.startActivityForResult(callerPlugin, cdigitIntent, CDIGIT_INTENT_REQUEST_CODE);
                    } catch (ActivityNotFoundException e) {
                    	Log.i(LOG_TAG, "CDIGIT n'est pas installé");
                    }
                }
            }
        );
    }
    
    /**
     * fonction appelee lors du retour de cdigit
     */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        
    	if (requestCode == CDIGIT_INTENT_REQUEST_CODE) {
        
    		if (data == null) {
                Log.i(LOG_TAG, "onActivityResult data NULL resultCode= " + resultCode);
                callbackContext.error("l'opération a été interrompu");
                return;
            }

            if (resultCode == Activity.RESULT_OK) {
                /*
                 * La transaction a pu commencer (mais elle peut être en echec).
                 *
                 * L'extra "CDIGIT_RESULT" est une string JSON
                 */
            	Log.i(LOG_TAG, "onActivityResult RESULT_OK : CDIGIT_RESULT="+data.getStringExtra("CDIGIT_RESULT"));
                callbackContext.success(data.getStringExtra("CDIGIT_RESULT"));
                return;
            }
            
            if (resultCode == Activity.RESULT_CANCELED) {
                /*
                 * La transaction n'a pas pu commencer.
                 * L'extra "CDIGIT_RESULT" est une string JSON qui contient :
                 * - le 'errorCode'
                 *
                 * Erreur possible :
                 * - Impossible de valider le token JWT (pas de connexion / erreur serveur)
                 * - Mauvais JWT : le serveur n'a pas validé le token (mauvais format / problème d'intégrité / expiré)
                 */
                Log.i(LOG_TAG, "onActivityResult RESULT_CANCELED : CDIGIT_RESULT="+data.getStringExtra("CDIGIT_RESULT"));
                callbackContext.error(data.getStringExtra("CDIGIT_RESULT"));
                return;
            }
            Log.i(LOG_TAG, "onActivityResult ERREUR INCONNUE");
            callbackContext.error("{\"errorCode\":\"ERREUR_INCONNUE\"}");
        }
    }
}
